package my.atherox.asteroidsnaturamil.GameObjects;

import my.atherox.asteroidsnaturamil.AsteroidsNaturaMil;
import my.atherox.asteroidsnaturamil.Renderable;
import my.atherox.asteroidsnaturamil.Updateable;

public class GameObject {

    float x, y, speed;

    public GameObject(float x, float y) {
        this.x = x;
        this.y = y;
        if (this instanceof Renderable) {
            AsteroidsNaturaMil.get().addRenderable((Renderable) this);
        }
        if (this instanceof Updateable) {
            AsteroidsNaturaMil.get().addUpdateables((Updateable) this);

        }
    }

    public void delete() {
        AsteroidsNaturaMil.get().delete(this);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getSpeed() {
        return speed;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }
}
