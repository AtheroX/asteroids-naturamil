package my.atherox.asteroidsnaturamil.GameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

import java.awt.Shape;
import java.util.ArrayList;

import my.atherox.asteroidsnaturamil.AsteroidsNaturaMil;
import my.atherox.asteroidsnaturamil.Renderable;
import my.atherox.asteroidsnaturamil.Updateable;

public class Nave extends GameObject implements Renderable, Updateable {

    private float x,y,speed, maxSpeed;
    private Polygon polygon;
    private float[] verts;
    private float width, height;

    private ArrayList<Bullet> bullets;

    private static Sound flash;

    private Sprite sprite;

    public Nave() {
        super(AsteroidsNaturaMil.get().getWidth()/2,AsteroidsNaturaMil.get().getHeight()/2);
        width = AsteroidsNaturaMil.get().getWidth();
        height = AsteroidsNaturaMil.get().getHeight();
        this.x = width/2;
        this.y = height/2;

        sprite = AsteroidsNaturaMil.ta.createSprite("Coche");
        sprite.scale(1.5f);
        sprite.scale(sprite.getScaleX()+sprite.getScaleY()/2*AsteroidsNaturaMil.get().scale);

        float sWidth = sprite.getWidth()*AsteroidsNaturaMil.get().xscale;
        float sHeight = sprite.getHeight()*AsteroidsNaturaMil.get().yscale;
        System.out.println(sWidth+" "+sHeight);
        verts = new float[]{
//                -sWidth/2, -sHeight/2,
//                sWidth/2, -sHeight/2,
//                sWidth/2, sHeight/2,
//                -sWidth/2, sHeight/2,
//                -sWidth/2, -sHeight/2
                -sHeight/2,-sWidth/2,
                sHeight/2, -sWidth/2,
                sHeight/2, sWidth/2,
                -sHeight/2, sWidth/2,
                -sHeight/2,-sWidth/2
        };

        polygon = new Polygon(verts);
        polygon.setOrigin(sWidth/2/1.5f,sHeight/2/1.5f);
        maxSpeed = 3.3f*AsteroidsNaturaMil.get().scale;
        bullets = new ArrayList<>();
        verts = polygon.getTransformedVertices();
        polygon.setPosition(width/2,height/2);
        if(flash==null)
            flash = Gdx.audio.newSound(Gdx.files.internal("camera.mp3")); //TODO sonido
    }

    public void move(){
        rotate();
        if(AsteroidsNaturaMil.controller.isUpPressed())
            speed += .75f*AsteroidsNaturaMil.get().scale;
        else if(speed < 0)
            speed = 0;
        else if(speed != 0)
            speed -= .04f*AsteroidsNaturaMil.get().scale; //linear drag

        if(speed > maxSpeed)
            speed = maxSpeed;

        x = (float) Math.cos(Math.toRadians(-polygon.getRotation())) * speed;
        y = (float) Math.sin(Math.toRadians(polygon.getRotation())) * speed;
        polygon.translate(x,y);
    }


    public void rotate(){
        if(AsteroidsNaturaMil.controller.isLeftPressed())
            polygon.rotate(2.7f);
        else if(AsteroidsNaturaMil.controller.isRightPressed())
            polygon.rotate(-2.7f);
    }

    public void wrapBorder() {
        x = polygon.getX();
        y = polygon.getY();
        if (x > width)
            x = 0;
        if (x < 0)
            x = width;
        if (y > height)
            y = 0;
        if (y < 0)
            y = height;
        polygon.setPosition(x,y);
    }

    public void shoot(){
        verts = polygon.getTransformedVertices();
        if(AsteroidsNaturaMil.controller.isShootPressed()){
            verts = polygon.getTransformedVertices();
            float sWidth = sprite.getWidth();
            float sHeight = sprite.getHeight();
            bullets.add(new Bullet(verts[4]-sWidth/1.5f,verts[5]-sHeight/1.5f, polygon.getRotation()));
            flash.play();
        }
    }

    public void checkCollision(){

        float x = verts[4], y = verts[5];
        for(Meteor m : AsteroidsNaturaMil.get().getMeteors()){
            if(distance(100,new Vector2(x,y), new Vector2(m.getRealX(), m.getRealY())) && m.hitable()){

                AsteroidsNaturaMil.get().getGm().loseLife();
                m.unHitable();
                m.hit();

                if(AsteroidsNaturaMil.get().getGm().getLifes()<=0)
                    delete();
            }
        }
    }

    boolean distance (float range, Vector2 v1, Vector2 v2){
        float deltax = v1.x-v2.x;
        float deltay = v1.y-v2.y;
        return deltax*deltax+deltay*deltay<range*range;
    }

    public ArrayList<Bullet> getBullets(){
        return bullets;
    }

    public float[] getVerts() {
        return verts;
    }


    @Override
    public void render() {
        sprite.setPosition(x,y);
        sprite.setRotation(polygon.getRotation()-90);
        sprite.draw(AsteroidsNaturaMil.batch);
    }

    @Override
    public void update() {
        move();
        wrapBorder();
        shoot();
        checkCollision();
    }



}
