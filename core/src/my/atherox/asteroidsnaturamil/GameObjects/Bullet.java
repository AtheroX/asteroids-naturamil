package my.atherox.asteroidsnaturamil.GameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;

import my.atherox.asteroidsnaturamil.AsteroidsNaturaMil;
import my.atherox.asteroidsnaturamil.Renderable;
import my.atherox.asteroidsnaturamil.Updateable;

public class Bullet extends GameObject implements Renderable, Updateable {

    private float rot, speed;
    private Sprite sprite;
    float textureChange = 0.1f, actualTexturechange = 0;
    int actualSprite = 1;

    public Bullet(float x, float y, float rot) {
        super(x, y);
        this.x = x;
        this.y = y;
        this.rot = rot;
        speed = 5f * AsteroidsNaturaMil.get().scale;
        sprite = AsteroidsNaturaMil.ta.createSprite("Flash"+actualSprite);
        sprite.setRotation(rot-90);

    }

    public void move() {
        x += Math.cos(Math.toRadians(-rot)) * speed;
        y += Math.sin(Math.toRadians(rot)) * speed;

        actualTexturechange += Gdx.graphics.getDeltaTime();
        if (actualTexturechange >= textureChange) {
            actualSprite = actualSprite == 1 ? 2 : 1;
            sprite = AsteroidsNaturaMil.ta.createSprite("Flash" + actualSprite);
            sprite.setScale(sprite.getScaleX()*AsteroidsNaturaMil.get().xscale, sprite.getScaleY()*AsteroidsNaturaMil.get().yscale);
            sprite.setRotation(rot-90);
            actualTexturechange = 0;
        }
    }

    public boolean wrap(){
        return x> AsteroidsNaturaMil.get().getWidth() || x< 0 || y > AsteroidsNaturaMil.get().getHeight() || y<0;
    }

    @Override
    public void delete() {
        super.delete();
        AsteroidsNaturaMil.get().getNave().getBullets().remove(this);
    }

    public void checkCollision(){
        for(int i = 0; i<AsteroidsNaturaMil.get().getMeteors().size(); i++){
            if(AsteroidsNaturaMil.get().getMeteors().get(i).contains(x,y)){
                AsteroidsNaturaMil.get().getGm().addScore(100);
                AsteroidsNaturaMil.get().getMeteors().get(i).hit();
                delete();
                break;
            }
        }
    }

    @Override
    public void render() {
        sprite.setPosition(x,y);

        sprite.draw(AsteroidsNaturaMil.batch);
    }

    @Override
    public void update() {
        move();
        if(wrap())
            delete();
        checkCollision();
    }
}
