package my.atherox.asteroidsnaturamil;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import my.atherox.asteroidsnaturamil.GameObjects.Meteor;
import my.atherox.asteroidsnaturamil.GameObjects.Nave;

public class AsteroidsNaturaMil extends ApplicationAdapter {

    private static AsteroidsNaturaMil instance;
    private OrthographicCamera cam;

    public static SpriteBatch batch;
    private BitmapFont font;

    public static TextureAtlas ta;
    Sprite bg;
    private float width;
    private float height;
    private GameManager gm;
    private Nave nave;
    private boolean gameOver, gameStarted = false;

    public static Controller controller;

    ArrayList<Updateable> updateables;
    ArrayList<Renderable> renderables;
    ArrayList<Meteor> meteors;

    ArrayList<Object> deleteables;

    public float xscale, yscale, scale;

    @Override
    public void create() {
        instance = this;
        gm = new GameManager();
        cam = new OrthographicCamera();
        cam.setToOrtho(false, 800, 480);

        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();

        batch = new SpriteBatch();
        ta = new TextureAtlas("spriteSheet3.txt");
        bg = new Sprite(new Texture("bg1.png"));
//        bg.setPosition(width/2, height/2);
        bg.setSize(width,height);

        xscale = width/800;
        yscale = height/480;
        scale = (xscale+yscale)/2;

        updateables = new ArrayList<>();
        renderables = new ArrayList<>();
        meteors = new ArrayList<>();
        deleteables = new ArrayList<>();
        gameOver = false;

        nave = new Nave();
        controller = new Controller();
        controller.changeSceneInput(true);

        fontGenerator();
        drawHud();
    }

    private void drawHud() {
        writeText("Level: "+gm.getLevel(), 10*scale,height-(20*scale));
        writeText("Score: "+gm.getScore(), 10*scale,height-(50*scale));
        writeText("Lifes: "+gm.getLifes(), width-(200*scale),height-(20*scale));
    }

    @Override
    public void render() {

        if(!gameStarted){
            Gdx.gl.glClearColor(.2f,.2f,.2f, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            cam.update();
            controller.drawMenu();
            if(controller.hasGameStart()) {
                gameStarted = true;
                controller.changeSceneInput(false);
            }
        }else {

            Gdx.gl.glClearColor(.155f, .585f, .208f, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            cam.update();
            batch.begin();
            bg.draw(batch);
            for (Renderable r : renderables) {
                r.render();
            }
            batch.end();
            update();
            if (gameOver)
                writeText("GameOver", width / 2, height / 2);
            else {
                drawHud();
                controller.draw();
            }

            realDelete();
        }
    }

    @Override
    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
        super.resize(width,height);
        controller.resize(width, height);
        xscale = width/800;
        yscale = height/480;
        scale = (xscale+yscale)/2;
    }
    private void realDelete() {
        for(Iterator it = deleteables.iterator(); it.hasNext();){
            Object o = it.next();
            if (o instanceof Renderable)
                renderables.remove((Renderable) o);

            if (o instanceof Updateable)
                updateables.remove((Updateable) o);

        }
    }

    public void update() {
        for (int i = 0; i < updateables.size(); i++) {
            try {
                updateables.get(i).update();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.getStackTrace();
            }

            checkGameOver();
        }
    }

    public void writeText(String text, float x, float y) {
        batch.setColor(1, 1, 1, 1);
        batch.begin();
        font.draw(batch, text, x, y);
        batch.end();
    }

    @Override
    public void dispose() {
        nave.delete();

    }

    public void addRenderable(Renderable r) {
        if (!renderables.contains(r))
            renderables.add(r);
    }

    public void addUpdateables(Updateable u) {
        if (!updateables.contains(u))
            updateables.add(u);
    }

    public void delete(Object o) {
        deleteables.add(o);
    }

    public static AsteroidsNaturaMil get() {
        return instance;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

	public ArrayList<Renderable> getRenderable() {
		return renderables;
	}

	public ArrayList<Meteor> getMeteors() {
		return meteors;
	}

	public Nave getNave(){
		return nave;
	}

	public GameManager getGm(){
		return gm;
	}

    public void fontGenerator() {
        FileHandle file = Gdx.files.internal("font/Hyperspace Bold.otf");
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(file);
        FreeTypeFontGenerator.FreeTypeFontParameter par = new FreeTypeFontGenerator.FreeTypeFontParameter();
        par.size = (int) (25*scale);
        font = generator.generateFont(par);
    }

    public void checkLevel() {
        if (meteors.isEmpty()) {
            gm.increaseLevel();
            gm.meteorQuant = gm.meteorQuant < 15 ? gm.meteorQuant + 2 : 15;
            spawnMeteor(gm.meteorQuant);
        }
    }

    public void spawnMeteor(int quant) {
        for (int i = 0; i < quant; i++) {
			Random r = new Random();
			boolean randomVertical = r.nextBoolean();
			float x = 0, y = 0;
			if (randomVertical) {
				x = r.nextInt((int)width) * 1f;
				y = r.nextBoolean() ? 0 : height;
			} else {
				y = r.nextInt((int)height) * 1f;
				x = r.nextBoolean() ? 0 : width;
			}
			meteors.add(new Meteor(x, y));
        }
    }

    public void checkGameOver(){
    	if(gm.getLifes()<=0 || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
    		gameover();

    	gm.haveWonLife();
    	checkLevel();
	}

	private void gameover() {
    	gameOver = true;
    	gm.setLife(0);
    	nave.delete();
    	gm.stopMusic();
    	if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER) || Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.isTouched()) {
            create();
            controller.changeSceneInput(false);
    	}
    	if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
    		System.exit(0);
	}
}
