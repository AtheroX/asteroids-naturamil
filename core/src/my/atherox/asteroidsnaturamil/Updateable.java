package my.atherox.asteroidsnaturamil;

public interface Updateable {
    public void update();
}
