package my.atherox.asteroidsnaturamil;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class GameManager {

    private Music music;
    int lifes, score, requiredScore, meteorQuant, level;

    public GameManager() {
        setMusic();
        this.lifes = 3;
        this.score = 0;
        this.requiredScore = 1000;
        this.meteorQuant = 0;
        this.level = 0;
    }

    private void setMusic() {
        music = Gdx.audio.newMusic(Gdx.files.internal("aves.mp3"));
        music.setLooping(true);
        music.play();
    }

    public void stopMusic(){
        music.stop();
    }

    public int getLifes() {
        return lifes;
    }

    public int getScore() {
        return score;
    }

    public int getRequiredScore() {
        return requiredScore;
    }

    public int getLevel() {
        return level;
    }

    public void increaseLevel() {
        level++;
    }

    public void loseLife(){
        lifes--;
    }

    public void setLife(int i){
        lifes = i;
    }

    public void haveWonLife(){
        if(score>=requiredScore){
            lifes++;
            requiredScore*=2;
        }
    }

    public void addScore(int score){
        this.score+=score;
    }

}
