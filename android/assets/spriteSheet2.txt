spriteSheet2.png
size: 32, 56
format: RGBA8888
filter: Nearest,Nearest
repeat: none
nave
  rotate: false
  xy: 1, 21
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
rock1
  rotate: false
  xy: 1, 39
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
rock2
  rotate: false
  xy: 16, 1
  size: 15, 15
  orig: 16, 16
  offset: 1, 0
  index: -1
shoot
  rotate: false
  xy: 1, 1
  size: 13, 18
  orig: 18, 18
  offset: 2, 0
  index: -1
