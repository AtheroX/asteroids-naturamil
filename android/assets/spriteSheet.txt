spriteSheet.png
size: 58, 32
format: RGBA8888
filter: Nearest,Nearest
repeat: none
Coche
  rotate: false
  xy: 1, 1
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
Pajaro11
  rotate: false
  xy: 25, 1
  size: 16, 13
  orig: 16, 13
  offset: 0, 0
  index: 1
Pajaro12
  rotate: false
  xy: 43, 1
  size: 14, 13
  orig: 14, 13
  offset: 0, 0
  index: 2
Pajaro21
  rotate: false
  xy: 25, 16
  size: 16, 13
  orig: 16, 13
  offset: 0, 0
  index: 1
Pajaro22
  rotate: false
  xy: 43, 16
  size: 14, 13
  orig: 14, 13
  offset: 0, 0
  index: 2
